#include <iostream>
#include <cuda_runtime.h>


__global__
void caesarEncode(int bufferSize, char *buffer, char *outbuffer, int shiftValue) {
	for (unsigned int i = 0u; i < bufferSize; i++) {
		outbuffer[i] = buffer[i] + shiftValue;
	}

}

int main(int argc, char *argv[])
{
	char* inBuffer;
	char* outBuffer;
	int N = 10;

	std::cout << "---------Test 1---------" << std::endl;
	cudaMallocManaged(&inBuffer, N * sizeof(char));
	cudaMallocManaged(&outBuffer, N * sizeof(char));
	strcpy(inBuffer,"helloworld");
	caesarEncode<<<1, 1>>>(N, inBuffer, outBuffer, 1);
	cudaDeviceSynchronize();
	std::cout << outBuffer << std::endl;
	if (strcmp(outBuffer, "ifmmpxpsme") == 0) {
		std::cout << "PASSED" << std::endl;
	}else {
		std::cout << "FAILED" << std::endl;
	}
	cudaFree(inBuffer);
	cudaFree(outBuffer);

	
	return 0;
}


